from django.shortcuts import render, get_object_or_404
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.utils import timezone
from django.views import generic

from .models import Question, Choice, Category


def index(request):
    # sinal de menos (-) para falar mostrar decrescente
    latest_question = Question.objects.filter(
        pub_date__lte=timezone.now()
        ).order_by('-pub_date')[:5]
    # template = loader.get_template('main/index.html')
    context = { 'latest_question': latest_question, }

    return render(request, 'main/index.html', context)

def results(request, question_id):
    question = get_object_or_404(Question, pk=question_id)
    return render(request, 'main/results.html', {'question': question})

def vote(request, question_id):
    question = get_object_or_404(Question, pk=question_id)
    try:
        selected_choice = question.choice_set.get(pk=request.POST['choice'])
    except (KeyError, Choice.DoesNotExist):
        return render(request, 'main/detail.html', {'question': question, 'error_message': "Você não selecionou uma opção"})
    else:
        selected_choice.votes += 1
        selected_choice.save()
        return HttpResponseRedirect(reverse('main:resultado', args=(question.id,)))

def category(request, category_id):
    category = get_object_or_404(Category, pk=category_id)
    return render(request, 'main/category.html', {'category': category})

class DetailView(generic.DetailView):
    model = Question
    template_name = 'main/detail.html'

    def get_queryset(self):
        return Question.objects.filter(pub_date__lte = timezone.now())