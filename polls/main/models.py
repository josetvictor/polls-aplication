from django.db import models
from django.utils import timezone
import datetime

# Create your models here.
class Question(models.Model):
    question_text = models.CharField(max_length=200)
    pub_date = models.DateTimeField('Data de publicação')
    def __str__(self):
        return self.question_text

    def was_published_recently(self):
        now = timezone.now()
        return now-datetime.timedelta(days=1) <= self.pub_date <= now

    def list_categories(self):
        return self.category_set.all()

class Choice(models.Model):
    question = models.ForeignKey(Question, on_delete=models.CASCADE)
    choice_text = models.CharField(max_length=200)
    votes = models.IntegerField(default=0)
    def __str__(self):
        return self.choice_text

class Category(models.Model):
    questions = models.ManyToManyField(Question)
    category_text = models.CharField(max_length=100)
    category_description = models.CharField(max_length=300)
    date_created = models.DateTimeField('Data de criação')
    def __str__(self):
        return self.category_text

    def list_polls(self):
        questions = self.questions.all()
        return questions.order_by('question_text')