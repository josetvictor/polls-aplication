from django.urls import path
from . import views

app_name = 'main'
urlpatterns = [
    path('', views.index, name='index'),
    path('question/<int:pk>/detail', views.DetailView.as_view(), name='detalhes'),
    path('question/<int:question_id>/results', views.results, name='resultado'),
    path('question/<int:question_id>/vote', views.vote, name='votacao'),
    path('question/<int:category_id>/categories', views.category, name='categorias'),
]