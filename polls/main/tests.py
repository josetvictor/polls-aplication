import datetime
from django.test import TestCase
from django.utils import timezone
from django.urls import reverse

from .models import Question

# Create your tests here.

class QuestionTests(TestCase):
    def test_was_published_recently_with_future_question(self):
        """
        o metodo deve retornar False para questões com data de publicação no futuro
        """

        data = timezone.now() + datetime.timedelta(seconds=1)
        question_future = Question(pub_date=data)
        self.assertIs(question_future.was_published_recently(), False)

    def test_was_published_recently_with_old_question(self):
        """
        o método deve retornar false para questões com
        data de publicação mais antigas que um dia
        """

        data = timezone.now()-datetime.timedelta(days=1,seconds=1)
        old_question = Question(pub_date=data)
        self.assertIs(old_question.was_published_recently(), False)

    def test_was_published_recently_with_recent_question(self):
        """
        o método deve retorar True para
        questões com data de publicação inferior a um dia
        """

        data = timezone.now()-datetime.timedelta(hours=23,minutes=59,seconds=59)
        recent_question = Question(pub_date = data)
        self.assertIs(recent_question.was_published_recently(), True)

# função para auxiliar a criação de dados no banco de testes
def create_question(text,days):
    """
    Função para criação de uma pergunta para texto e data de publicação
    """

    data = timezone.now() + datetime.timedelta(days=days)
    return Question.objects.create(question_text=text, pub_date = data)

class IndexViewTest(TestCase):
    def test_no_questions(self):
        """
        se não existirem questões é exibida uma mensagem especifica
        """

        response = self.client.get(reverse('main:index'))
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "Nenhuma questão disponível.")
        self.assertQuerysetEqual(response.context['latest_question'], [])

    def test_past_question(self):
        """
        questões com data de publicação no passado são exibidas na index
        """
        create_question(text="Questão no passado.", days= -30)
        response = self.client.get(reverse('main:index'))
        self.assertQuerysetEqual(
            response.context['latest_question'],
            ['<Question: Questão no passado.>']
        )

    def test_future_question(self):
        """
        Perguntas com data de publicação no futuro não devem ser exibidas
        """

        create_question(text="Questão no futuro.", days = 30)
        response = self.client.get(reverse('main:index'))
        self.assertContains(response, "Nenhuma questão disponível.")
        self.assertQuerysetEqual(response.context['latest_question'],[])

    def test_future_question_and_past_question(self):
        """
        Perguntas com data de publicação no passado são exibidas e com data de publicaçãono futuro são omitidas
        """

        create_question(text="Questões no passado",days=-30)
        create_question(text="Questões no futuro",days=30)
        response = self.client.get(reverse('main:index'))
        self.assertQuerysetEqual(
            response.context['latest_question'],
            ['<Question: Questões no passado>']
        )

    def test_two_past_questions(self):
        """
        Exibe normalmente mais de uma pergunta com data de publicação no passado
        """

        create_question(text="Questões no passado 1.",days=-1)
        create_question(text="Questões no passado 2.",days=-5)
        response = self.client.get(reverse('main:index'))
        self.assertQuerysetEqual(
            response.context['latest_question'],
            ['<Question: Questões no passado 1.>',
            '<Question: Questões no passado 2.>']
        )

class QuestionDetailView(TestCase):
    def test_future_question(self):
        """
        deverá retornar um erro 404 para questões com data no futuro
        """
        future_question = create_question(text="Questão futuro.", days = 5)
        url = reverse('main:detalhes', args=(future_question.id,))
        response = self.client.get(url)
        self.assertEqual(response.status_code, 404)